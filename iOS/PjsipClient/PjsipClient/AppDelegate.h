//
//  AppDelegate.h
//  PjsipClient
//
//  Created by Igor Nazarov on 12/12/17.
//  Copyright © 2017 Igor Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (BOOL)isIPv6;

@end

