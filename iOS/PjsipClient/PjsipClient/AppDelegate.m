//
//  AppDelegate.m
//  PjsipClient
//
//  Created by Igor Nazarov on 12/12/17.
//  Copyright © 2017 Igor Nazarov. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"
#import "XCPjsua.h"
#import "SimplePing.h"

#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IOS_VPN         @"utun0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"

typedef NS_ENUM(int, IpVersion)
{
    IPvNotDetermined,
    IPv4,
    IPv6
};

@interface AppDelegate () <SimplePingDelegate>
{
    Reachability* reachability;
    SimplePing* pinger;
}

@property (nonatomic) IpVersion ipVersion;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self startReachability];
    
    [[XCPjsua sharedXCPjsua] startPjsip];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Reachability

- (void)startReachability
{
    // Allocate a reachability object
    reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Set the blocks
    reachability.reachableBlock = ^(Reachability*reach)
    {
        // keep in mind this is called on a background thread
        // and if you are updating the UI it needs to happen
        // on the main thread, like this:
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"REACHABLE!");
        });
    };
    
    reachability.unreachableBlock = ^(Reachability*reach)
    {
        NSLog(@"UNREACHABLE!");
    };
    
    // Here we set up a NSNotification observer. The Reachability that caused the notification
    // is passed in the object parameter
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [reachability startNotifier];
}

- (void)reachabilityChanged:(NSNotification*)nf
{
    if (reachability.isReachable)
    {
        [self detectIpVersion];
    }
}

#pragma mark - SimplePingDelegate

- (void)simplePing:(SimplePing *)_pinger didStartWithAddress:(NSData *)address
{
    [pinger sendPingWithData:nil];
}

- (void)simplePing:(SimplePing *)pinger didFailWithError:(NSError *)error
{
    
}

- (void)simplePing:(SimplePing *)pinger didSendPacket:(NSData *)packet sequenceNumber:(uint16_t)sequenceNumber
{
    [self setIpVersion:IPv6];
}

- (void)simplePing:(SimplePing *)pinger didFailToSendPacket:(NSData *)packet sequenceNumber:(uint16_t)sequenceNumber error:(NSError *)error
{
    [self setIpVersion:IPv4];
}

- (void)simplePing:(SimplePing *)pinger didReceivePingResponsePacket:(NSData *)packet sequenceNumber:(uint16_t)sequenceNumber
{
    
}

- (void)simplePing:(SimplePing *)pinger didReceiveUnexpectedPacket:(NSData *)packet
{
    
}

#pragma mark - IP address handling

- (BOOL)isIPv6
{
    return _ipVersion == IPv6;
}

- (void)setIpVersion:(IpVersion)ipVersion
{
    BOOL isFirstDetection = (_ipVersion == IPvNotDetermined);
    
    _ipVersion = ipVersion;
    
    if (!isFirstDetection)
    {
        [[XCPjsua sharedXCPjsua] changeIp];
    }
}

- (void)ping:(NSString*)address
{
    pinger = [[SimplePing alloc] initWithHostName:address];
    pinger.delegate = self;
    [pinger start];
}

- (void)detectIpVersion
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableDictionary *addresses = [NSMutableDictionary dictionary];
        
        // retrieve the current interfaces - returns 0 on success
        struct ifaddrs *interfaces;
        if (!getifaddrs(&interfaces))
        {
            // Loop through linked list of interfaces
            struct ifaddrs *interface;
            for (interface=interfaces; interface; interface=interface->ifa_next)
            {
                if (!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ )
                {
                    continue; // deeply nested code harder to read
                }
                
                const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
                char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
                
                if (addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6))
                {
                    NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                    NSString *type;
                    
                    if (addr->sin_family == AF_INET)
                    {
                        if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN))
                        {
                            type = IP_ADDR_IPv4;
                        }
                    }
                    else
                    {
                        const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                        if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN))
                        {
                            type = IP_ADDR_IPv6;
                        }
                    }
                    
                    if (type)
                    {
                        NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                        addresses[key] = [NSString stringWithUTF8String:addrBuf];
                    }
                }
            }
            // Free memory
            freeifaddrs(interfaces);
        }
        
        NSLog(@"Network interfaces:\n %@", addresses);
        
        __block NSString* ipv6 = nil;
        
        // Search for WiFi interface which is "en0"
        [addresses enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([key isEqualToString:@"en0/ipv6"])
            {
                ipv6 = obj;
                *stop = YES;
            }
        }];
        
        if (ipv6)
        {
            [self ping:ipv6];
        }
        else
        {
            // Search for cellular interface which is "pdp_ip0"
            [addresses enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                if ([key isEqualToString:@"pdp_ip0/ipv6"])
                {
                    ipv6 = obj;
                    *stop = YES;
                }
            }];
            
            if (ipv6)
            {
                [self ping:ipv6];
            }
            else
            {
                self.ipVersion = IPv4;
            }
        }
    });
}

@end
