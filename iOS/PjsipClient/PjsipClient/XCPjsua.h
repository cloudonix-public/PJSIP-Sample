/*
 * Copyright (C) 2014 Xianwen Chen <xianwen@xianwenchen.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#import <Foundation/Foundation.h>

typedef NS_ENUM(int, CallState)
{
    CallStateIncoming = 2,
    CallStateConfirmed = 5,
    CallStateDisconnected = 6
};

@protocol XCPjsuaListener <NSObject>

@optional

- (void)onRegState:(BOOL)isRegistered;
- (void)onCallState:(CallState)callState callId:(int)callId;

@end

@interface XCPjsua : NSObject

/**
 * Get the singleton XCPjsua.
 */
+ (XCPjsua *)sharedXCPjsua;

/**
 * Register and unregister listeners.
 */
- (void)addListener:(id<XCPjsuaListener>)listener;
- (void)removeListener:(id<XCPjsuaListener>)listener;

/**
 * Initialize and start pjsua.
 */
- (void)startPjsip;

/**
 * Register the account on sip server.
 */
- (void)registerOnServer:(char *)sipDomain withUserName:(char *)sipUser andPassword:(char *)password;

/**
 * VoIP call.
 */
- (void)makeCallTo:(char*)destUri;
- (void)answerCall:(int)callID;
- (void)hangupCall:(int)callID;
- (void)endCall;

/**
 * Handle IP address change
 */
- (void)changeIp;

@end
