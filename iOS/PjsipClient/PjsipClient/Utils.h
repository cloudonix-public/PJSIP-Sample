//
//  Utils.h
//  PjsipClient
//
//  Created by Igor Nazarov on 12/18/17.
//  Copyright © 2017 Igor Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (NSString*)getServer;
+ (NSString*)getPassword;
+ (NSString*)getPort;

@end
