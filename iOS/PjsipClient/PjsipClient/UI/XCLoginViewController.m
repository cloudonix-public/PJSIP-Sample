/*
 * Copyright (C) 2014 Xianwen Chen <xianwen@xianwenchen.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#import "XCLoginViewController.h"
#import "XCPjsua.h"
#import "XCOutgoingCallController.h"
#import "Utils.h"

@interface XCLoginViewController () <UITextFieldDelegate, XCPjsuaListener>

@property (nonatomic, weak) IBOutlet UITextField* textField;
@property (nonatomic, weak) IBOutlet UILabel* registerStateLabel;

@end

@implementation XCLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    [[XCPjsua sharedXCPjsua] addListener:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[XCPjsua sharedXCPjsua] removeListener:self];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender
{
    NSString* serverStr = [Utils getServer];
    NSString* passwordStr = [Utils getPassword];
    
    char* username = (char*)self.textField.text.UTF8String;
    char* server = (char*)serverStr.UTF8String;
    char* password = (char*)passwordStr.UTF8String;
    
    [[XCPjsua sharedXCPjsua] registerOnServer:server withUserName:username andPassword:password];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - XCPjsuaListener

- (void)onRegState:(BOOL)isRegistered
{
    self.registerStateLabel.text = isRegistered ? @"Registered" : @"Not registered";
    
    if (isRegistered)
    {
        [self showOutgoingCallScreen];
    }
}

#pragma mark - Utils

- (void)showOutgoingCallScreen
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NSString* screenID = NSStringFromClass([XCOutgoingCallController class]);
    
    XCOutgoingCallController* outgoingCallScreen = [storyboard instantiateViewControllerWithIdentifier:screenID];
    [self.navigationController pushViewController:outgoingCallScreen animated:YES];
}

@end
