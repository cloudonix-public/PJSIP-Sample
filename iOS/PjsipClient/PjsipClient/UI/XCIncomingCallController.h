//
//  XCIncomingCallController.h
//  simpleVoIP
//
//  Created by Igor Nazarov on 12/27/16.
//  Copyright © 2016 Xianwen Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCIncomingCallController : UIViewController

@property (nonatomic) int callID;

@end
