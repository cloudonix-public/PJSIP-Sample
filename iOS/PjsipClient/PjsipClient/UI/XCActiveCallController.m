//
//  XCActiveCallController.m
//  simpleVoIP
//
//  Created by Igor Nazarov on 12/27/16.
//  Copyright © 2016 Xianwen Chen. All rights reserved.
//

#import "XCActiveCallController.h"
#import "XCPjsua.h"

@interface XCActiveCallController () <XCPjsuaListener>

@end

@implementation XCActiveCallController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    [[XCPjsua sharedXCPjsua] addListener:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[XCPjsua sharedXCPjsua] removeListener:self];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)hangup:(id)sender
{
    [[XCPjsua sharedXCPjsua] hangupCall:self.callID];
}

#pragma mark - XCPjsuaListener

- (void)onCallState:(CallState)callState callId:(int)callId
{
    if (callState == CallStateDisconnected)
    {
        UIViewController* outgoingCallScreen = self.navigationController.viewControllers[1];
        [self.navigationController popToViewController:outgoingCallScreen animated:YES];
    }
}

@end
