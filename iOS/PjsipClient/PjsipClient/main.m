//
//  main.m
//  PjsipClient
//
//  Created by Igor Nazarov on 12/12/17.
//  Copyright © 2017 Igor Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
