//
//  Utils.m
//  PjsipClient
//
//  Created by Igor Nazarov on 12/18/17.
//  Copyright © 2017 Igor Nazarov. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (NSString*)getServer
{
    NSDictionary* configs = [self getConfigs];
    return configs[@"server"];
}

+ (NSString*)getPassword
{
    NSDictionary* configs = [self getConfigs];
    return configs[@"password"];
}

+ (NSString*)getPort
{
    NSDictionary* configs = [self getConfigs];
    return configs[@"port"];
}

+ (NSDictionary*)getConfigs
{
    NSString* configsPath = [[NSBundle mainBundle] pathForResource:@"configs" ofType:@"plist"];
    NSDictionary* configs = [NSDictionary dictionaryWithContentsOfFile:configsPath];
    return configs;
}

@end
