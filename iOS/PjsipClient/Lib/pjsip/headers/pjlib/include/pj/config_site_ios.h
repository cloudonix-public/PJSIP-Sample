#define PJ_CONFIG_IPHONE 1
#define PJMEDIA_HAS_OPUS_CODEC          1
#define PJ_HAS_SSL_SOCK 0
#define USE_WEBRTC 1
#define PJMEDIA_HAS_SRTP 0
#define PJ_HAS_IPV6 1

#include <pj/config_site_sample.h>
